#BSUB -J myjob2           # sets the job name to myjob1.
#BSUB -L /bin/bash        # uses the bash login shell to initialize the job's execution environment.
#BSUB -W 00:01            # sets to 12.5 hours the job's runtime wall-clock limit.
#BSUB -n 2                # assigns 3 cores for execution.
#BSUB -R "span[ptile=2]"  # assigns 3 cores per node.
#BSUB -R "rusage[mem=100]"  # reserves 5000MB per process/CPU for the job (i.e., 15,000 MB for job/node)
#BSUB -M 100              # sets to 5,000MB (~5GB) the per process enforceable memory limit.
#BSUB -o stdout1.%J       # directs the job's standard output to stdout1.jobid
#
module load intel FFTW GSL
#
export OMP_NUM_THREADS=1
mpiexec.hydra -np 2 ./bin/volution
##
