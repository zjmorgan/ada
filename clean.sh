#!/bin/bash

start=1
stop=100
step=1

for i in `seq $start $step $stop`; do
	rm vtk/*.$i.vtk put/*.$i.dat .dat/*.$i.dat;
done
