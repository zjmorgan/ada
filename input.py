#!/usr/bin/env python

import matplotlib as mpl
mpl.use("pgf")
pgf_with_pdflatex = {
    "font.family": "serif",
    "text.usetex": True,
    "pgf.rcfonts": False,
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
        r"\usepackage[T1]{fontenc}",
        r"\usepackage{libertine}",
        r"\usepackage[libertine]{newtxmath}",
        r"\usepackage[scientific-notation=true]{siunitx}"
        ]
}
mpl.rcParams.update(pgf_with_pdflatex)

import numpy as np
#import matplotlib.pyplot as plt
#
from scipy.ndimage.filters import gaussian_filter

n0, n1 = 256, 256
out = np.zeros((n0,n1))
out = np.reshape(out, (n0*n1))

#out.tofile('.dat/charge.density.0.dat')
#out.tofile('.dat/conductivity.2.0.dat')
#out.tofile('.dat/conductivity.3.0.dat')
#out.tofile('.dat/conductivity.4.0.dat')
#out.tofile('.dat/conductivity.5.0.dat')
#
out = np.reshape(out, (n0,n1))
for i in range(0,n0):
    for j in range(0,n1):
#        if ((i-n0/2)**2+(j-n1/2)**2 > (n0/8)**2):
        if ((i-n0/2)**2/float(n0/8)**2+(j-n1/2)**2/float(n1/8)**2 < 1.0):
            out[i,j] = 1.0

#for i in range(0,n0):
#    for j in range(0,n1):
#        if ((i < n0/3 or i > 2*n0/3) and j > n1/5 and j < 2*n1/5):
#            out[i,j] = 1.0
#        if (((i > 2*n0/9 and i < n0/3) or (i > 2*n0/3 and i < 7*n0/9)) and j >= 2*n1/5 and j <= 3*n1/5):
#            out[i,j] = 1.0
#        if (i > 2*n0/9 and i < 7*n0/9 and j > 3*n1/5 and j < 4*n1/5):
#            out[i,j] = 1.0

#out = np.reshape(out, (n0*n1))
#out.tofile('.dat/conductivity.0.0.dat')
#out.tofile('.dat/conductivity.1.0.dat')
##
#out -= 0.5
# 
#out.tofile('.dat/concentration.0.dat')

##for i in range(0,n0):
##    for j in range(0,n1):
##        out[i,j]= np.exp(-((2*np.pi*i/n0-np.pi+np.pi/5)**2+(2*np.pi*j/n1-np.pi+np.pi/5)**2)/0.3)-np.exp(-((2*np.pi*i/n0-np.pi-np.pi/5)**2+(2*np.pi*j/n1-np.pi+np.pi/5)**2)/0.2)+np.exp(-((2*np.pi*i/n0-np.pi-np.pi/5)**2+(2*np.pi*j/n1-np.pi-np.pi/5)**2)/0.4)
##        if ((i-n0/4)**2+(j-n1/2)**2 < (128/8)**2):
##            out[i,j] = 0.5
##        if ((i-3*n0/4)**2+(j-n1/2)**2 < (128/8)**2):
##            out[i,j] = -0.5
#
#
#out = gaussian_filter(out, 3, order=0, output=None, mode='reflect', cval=0.0)      
#
#out = np.random.normal(0, 1, n0*n1)
out = gaussian_filter(out, 3, order=0, output=None, mode='reflect', cval=0.0)      

out = np.reshape(out, (n0*n1))
out.tofile('.dat/displacement.0.0.dat')
out.tofile('.dat/displacement.1.0.dat')

out = np.zeros((n0,n1))

out = np.reshape(out, (n0*n1))
out.tofile('.dat/displacement.2.0.dat')
#out.tofile('.dat/velocity.1.0.dat')
#out.tofile('.dat/vorticity.0.0.dat')
#out.tofile('.dat/vorticity.1.0.dat')
#out.tofile('.dat/velocity.2.0.dat')

#for i in range(0,n0):
#    for j in range(0,n1):
#        if (i < n0/4):
#            out[i,j] = 0.4
#        elif (i > 3*n0/4):
#            out[i,j] = 0.6
#        else:
#            if (j < n1/2):
#                out[i,j] = 0.2
#            else:
#                out[i,j] = 0.8
#     
#out = gaussian_filter(out, 5, order=0, output=None, mode='reflect', cval=0.0)      
#
#out = np.reshape(out, (n0*n1))
#
#out.tofile('.dat/conductivity.0.0.dat')
#
#out = np.zeros((n0,n1))
#
#for i in range(0,n0):
#    for j in range(0,n1):
#        if (i < n0/4):
#            out[i,j] = 0.0
#        elif (i > 3*n0/4):
#            out[i,j] = 0.0
#        else:
#            if (j < n1/2):
#                out[i,j] = 0.0
#            else:
#                out[i,j] = 0.0

#out.tofile('.dat/concentration.0.dat')

#out = np.zeros((n0*n1))
#out.tofile('.dat/conductivity.2.0.dat')
#out.tofile('.dat/conductivity.3.0.dat')
#out.tofile('.dat/conductivity.4.0.dat')
#out.tofile('.dat/conductivity.5.0.dat')
#
#out += 1.0
#out.tofile('.dat/charge.density.0.dat')
#out.tofile('.dat/electrical.state.dat')
