.phony: all clean

program = HUH_mastercode_T_ADA
subprograms = ohm
solvers = charge.conduction

vtk = visualization
dft = discrete.fourier.transform
rng = random.number.generator
io = input.output
sf = standard.functions

interact = interaction
interface = interfacial

vizualize = bin/$(vtk)
executable = bin/$(program)

dependencies = $(program) \
               $(subprograms) \
               $(dft) \
               $(rng) \
               $(io) \
               $(sf) \
               $(solvers) \
               $(interact) \
               $(interface) 

o = $(addsuffix .o, $(dependencies))
objects = $(addprefix obj/, $(o))

m = $(addsuffix .o, $(subprograms))
modules = $(addprefix obj/, $(m))

sys = $(shell hostname -f)

ifneq (, $(findstring pc, $(sys)))

  f90 = /bin/mpif90
  cpp = /bin/g++
  c = /bin/mpicc

  f90flags = -fopenmp
  cppflags = -fopenmp
  cflags = -fopenmp
  
  inc = -I/usr/local/include
  lib = -L/usr/local/lib
  mod = -J

  libraries = -lfftw3_mpi -lfftw3_omp -lfftw3 -lmpi -lgsl -lgslcblas -lm

else ifneq (, $(findstring gibbs, $(sys)))
  
  f90 = mpif90
  cpp = g++
  c = mpicc
 
  f90flags = -fopenmp -g -pg -Wall -O2
  cppflags = -fopenmp -g -pg -Wall -O2
  cflags = -fopenmp -g -pg -Wall -O2
  
  inc = -I/usr/include -I/usr/include/mpi
  lib = -L/usr/lib
  mod = -J

  libraries = -lfftw3_mpi -lfftw3_omp -lfftw3 -lmpi -lgsl -lcblas -latlas -lm


else ifneq (, $(findstring stampede, $(sys)))

  f90 = mpif90
  cpp = mpicxx
  c = mpicc

  f90flags = -openmp -g -O2 -xhost#-simd
  cppflags = -fopenmp -g -O2
  cflags = -openmp -g -O2 -xhost#-simd 
  
  inc = -I$$TACC_FFTW3_INC -I$$TACC_GSL_INC -I$$TACC_GSL_INC/gsl
  lib = -Wl,-rpath,$$TACC_FFTW3_LIB -L$$TACC_FFTW3_LIB -L$$TACC_GSL_LIB 
  mod = -module

  libraries = -lfftw3_mpi -lfftw3_omp -lfftw3 -lgsl -lgslcblas

else  
  
  f90 = mpiifort
  cpp = icpc
  c = mpiicc
 
  f90flags = -openmp -g -O2
  cppflags = -openmp -g -O2
  cflags = -openmp -g -O2
  
  inc = -I/usr/include
  lib = -L/usr/lib
  mod = -module
 
  libraries = -lfftw3_mpi -lfftw3_omp -lfftw3 -lgsl -lgslcblas -lm

endif

all: $(executable) $(vizualize)

$(executable): $(objects)
	$(f90) \
	$(f90flags) \
	-Imod \
	$(inc) \
	$(lib) \
	$(objects) \
	-o $(executable) \
	$(libraries) 

bin/%: src/%.cpp
	$(cpp) \
	$(cppflags) \
	$(inc) \
	$(lib) \
	$< \
	-o $@ \
	$(libraries)

# -----------------------------------------------------------------------------

obj/$(program).o: src/$(program).f90 \
                  obj/$(interface).o \
                  obj/$(interact).o \
                  $(modules)
	$(f90) \
	$(f90flags) \
	-Imod $(inc) $(lib) \
	-c src/$(program).f90 \
	-o obj/$(program).o \
	$(libraries)
