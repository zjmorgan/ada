#!/usr/bin/env python

#import matplotlib as mpl
#mpl.use("pgf")
#pgf_with_pdflatex = {
#    "font.family": "serif",
#    "text.usetex": True,
#    "pgf.rcfonts": False,
#    "pgf.texsystem": "pdflatex",
#    "pgf.preamble": [
#        r"\usepackage[T1]{fontenc}",
#        r"\usepackage{libertine}",
#        r"\usepackage[libertine]{newtxmath}",
#        r"\usepackage[scientific-notation=true]{siunitx}",
#        r"\usepackage{physics}"
#        ]
#}
#mpl.rcParams.update(pgf_with_pdflatex)

import numpy as np
import matplotlib.pyplot as plt
#import seaborn as sns
#sns.set(style='ticks', font_scale=2, palette='Set2')
#sns.despine()

import glob  
path = 'put/*.energy.*.dat'   
files = glob.glob(path)

files.sort()

for i in np.arange(len(files)):
   
    t, bulk, interfacial, total = np.loadtxt(files[i], comments='#', unpack=True)

    plt.figure()
    plt.plot(t, total, '-', alpha=0.5)
    plt.plot(t, bulk, '-', alpha=0.5)
    plt.plot(t, interfacial, '-', alpha=0.5)
    plt.ticklabel_format(axis='both', style='sci', scilimits=(0,0))
    plt.xlabel(r'Time, $t^*$')
    plt.ylabel(r'Energy, $F^*$')
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,y1,y2))
    plt.savefig('get/output.'+str(i)+'.pdf', bbox_inches='tight')