program revolution

  use, intrinsic :: iso_c_binding

  use interfacial
  use interaction
  use ohm

  implicit none

  call solver_initialize()
  
  call ohm_initialize()
  
  call ohm_evolve()

  call ohm_finalize()
  
  call solver_finalize()

end program
