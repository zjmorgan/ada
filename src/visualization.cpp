#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main (int argc, char *argv[])
{

  vector<string> list, scalars, vectors, tensors;
  int lines = 0;

  string line;
  ifstream input ("get/transfer.dat");
  while ( getline (input, line) )
    {
      istringstream ss(line);
      string token;
      getline(ss, token, '|');
      if ( line[0] != '#' && line.empty() != 1 )
        {
          while ( getline(ss, token, '|') )
            {
              token.erase(remove(token.begin(), token.end(), '\t'), token.end());
              token.erase(remove(token.begin(), token.end(), ' '), token.end());
              if (lines <= 5 && token != "NULL")
                { 
                  list.push_back(token);
                }
              if (lines == 6 && token != "NULL")
                {
                  scalars.push_back(token);
                }
              if (lines == 7 && token != "NULL")
                {
                  vectors.push_back(token);
                }
              if (lines == 8 && token != "NULL")
                {
                  tensors.push_back(token);
                }
            }
          ++lines;
        }
    }
  input.close();

  int n0;
  int n1;
  int n2;
  int start;
  int stop;
  int skip;
  istringstream (list.at(0)) >> n0;
  istringstream (list.at(1)) >> n1;
  istringstream (list.at(2)) >> n2;
  istringstream (list.at(3)) >> start;
  istringstream (list.at(4)) >> stop;
  istringstream (list.at(5)) >> skip;

  cout << n0 << "x" << n1 << "x" << n2 << "\n";

  int size = n0*n1*n2;
  double *scalar = new double[size];
  double *vector_0 = new double[size];
  double *vector_1 = new double[size];
  double *vector_2 = new double[size];
  double *tensor_0 = new double[size];
  double *tensor_1 = new double[size];
  double *tensor_2 = new double[size];
  double *tensor_3 = new double[size];
  double *tensor_4 = new double[size];
  double *tensor_5 = new double[size];

  for (int l = start; l <= stop; l += skip)
    {
      stringstream ss;
      ss << l;
      string step = ss.str();

      string output = "vtk/result.";

      output.append(step);
      output.append(".vtk");

      cout << output << "\n";

      ofstream outfile;
      outfile.open(output.c_str(), ios::out | ios::binary);
      outfile << "# vtk DataFile Version 2.0" << '\n';
      outfile << "Phase Field Simulation Results" << "\n";
      outfile << "ASCII" << "\n";
      outfile << "DATASET STRUCTURED_POINTS" << "\n";
      outfile << "DIMENSIONS " << n0 << " " << n1 << " " << n2 << "\n";
      outfile << "ORIGIN 0 0 0" << "\n";
      outfile << "SPACING 1 1 1" << "\n";
      outfile << "POINT_DATA " << size << "\n";

      string result;
      ifstream infile;

      for (unsigned m = 0; m < scalars.size(); ++m)
        {
          result.clear();
          result.append(".dat/");
          result.append(scalars.at(m));
          result.append(".");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(scalar), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          outfile << "SCALARS" << " " << scalars.at(m) << " " << "double" << "\n";
          outfile << "LOOKUP_TABLE default" << "\n";

          for (int k = 0; k < n2; ++k)
            {
              for (int j = 0; j < n1; ++j)
                {
                  for (int i = 0; i < n0; ++i)
                    {
                      outfile << scalar[k+n2*(j+n1*i)] << "\n";
                    }
                }
            }
        }

       for (unsigned n = 0; n < vectors.size(); ++n)
        {
          result.clear();
          result.append(".dat/");
          result.append(vectors.at(n));
          result.append(".0.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(vector_0), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          result.clear();
          result.append(".dat/");
          result.append(vectors.at(n));
          result.append(".1.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(vector_1), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          result.clear();
          result.append(".dat/");
          result.append(vectors.at(n));
          result.append(".2.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(vector_2), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          outfile << "VECTORS" << " " << vectors.at(n) << " " << "double" << "\n";

          for (int k = 0; k < n2; ++k)
            {
              for (int j = 0; j < n1; ++j)
                {
                  for (int i = 0; i < n0; ++i)
                    {
                      outfile << vector_0[k+n2*(j+n1*i)] << " " << vector_1[k+n2*(j+n1*i)] << " " << vector_2[k+n2*(j+n1*i)] << "\n";
                    }
                }
            }
        }

       for (unsigned p = 0; p < tensors.size(); ++p)
        {
          result.clear();
          result.append(".dat/");
          result.append(tensors.at(p));
          result.append(".0.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(tensor_0), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          result.clear();
          result.append(".dat/");
          result.append(tensors.at(p));
          result.append(".1.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(tensor_1), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          result.clear();
          result.append(".dat/");
          result.append(tensors.at(p));
          result.append(".2.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(tensor_2), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          result.clear();
          result.append(".dat/");
          result.append(tensors.at(p));
          result.append(".3.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(tensor_3), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          result.clear();
          result.append(".dat/");
          result.append(tensors.at(p));
          result.append(".4.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(tensor_4), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          result.clear();
          result.append(".dat/");
          result.append(tensors.at(p));
          result.append(".5.");
          result.append(step);
          result.append(".dat");

          infile.clear();
          infile.open(result.c_str(), ios::in | ios::binary);
          infile.read(reinterpret_cast<char*>(tensor_5), size*sizeof(double));
          infile.close();

          cout << "  " << result << "\n";

          outfile << "TENSORS" << " " << tensors.at(p) << " " << "double" << "\n";

          for (int k = 0; k < n2; ++k)
            {
              for (int j = 0; j < n1; ++j)
                {
                  for (int i = 0; i < n0; ++i)
                    {
                      outfile << tensor_0[k+n2*(j+n1*i)] << " " << tensor_5[k+n2*(j+n1*i)] << " " << tensor_4[k+n2*(j+n1*i)] << "\n";
                      outfile << tensor_5[k+n2*(j+n1*i)] << " " << tensor_1[k+n2*(j+n1*i)] << " " << tensor_3[k+n2*(j+n1*i)] << "\n";
                      outfile << tensor_4[k+n2*(j+n1*i)] << " " << tensor_3[k+n2*(j+n1*i)] << " " << tensor_2[k+n2*(j+n1*i)] << "\n";
                    }
                }
            }
        }
    outfile.close();
    }

  delete [] scalar;
  delete [] vector_0;
  delete [] vector_1;
  delete [] vector_2;
  delete [] tensor_0;
  delete [] tensor_1;
  delete [] tensor_2;
  delete [] tensor_3;
  delete [] tensor_4;
  delete [] tensor_5;

  return 0;
}
