program volution

  use, intrinsic :: iso_c_binding

  use interference

  implicit none

  integer(c_int) :: i

  call solver_initialize()
  
    call fick_initialize()
    call ohm_initialize()

     do i = 1, 10

        call fick_evolve()
        call fick_ohm_couple()

        call ohm_evolve()
        call ohm_fick_couple()

      end do

      call fick_write()
      call ohm_write()

    call fick_finalize()
    call ohm_finalize()
  
  call solver_finalize()

end program
