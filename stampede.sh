#!/bin/bash
# ---------------------------------------------------
# Example SLURM job script to run hybrid applications
# (MPI/OpenMP or MPI/pthreads) on TACC's Stampede
# system.
# ---------------------------------------------------
#SBATCH -J hybrid           # Job name
#SBATCH -o put/hybrid.o     # Name of stdout output file(%j expands to jobId)
#SBATCH -e put/hybrid.o     # Name of stderr output file(%j expands to jobId)
#SBATCH -p normal           # Submit to the 'normal' or 'development' queue
#SBATCH -N 1                # Total number of nodes requested (16 cores/node)
#SBATCH -n 1                # Total number of mpi tasks requested
#SBATCH -t 00:00:30         # Run time (hh:mm:ss) - 1.5 hours
# The next line is required if the user has more than one project
#SBATCH -A TG-DMR150011  # Allocation name to charge job against

# This example will run a hybrid application using 16 threads

# Set the number of threads per task(Default=1)
export OMP_NUM_THREADS=16

# Run the hybrid application
ibrun tacc_affinity ./bin/revolution
