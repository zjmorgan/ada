#!/usr/bin/env python

import numpy as np

n0, n1, n2 = 101, 101, 1

out = np.zeros((n0,n1,n2))             
out = np.reshape(out, (n0*n1*n2))

out.tofile('.dat/conductivity.2.0.dat')
out.tofile('.dat/conductivity.3.0.dat')
out.tofile('.dat/conductivity.4.0.dat')
out.tofile('.dat/conductivity.5.0.dat')

string = np.loadtxt('.dat/sigma_i.dat', dtype=str, skiprows=1, usecols=(2,))

for i in range(0,n0):
    for j in range(0,n1):
        for k in range(0,n2):
            value = string[k+n2*(j+n1*i)] 
            out[k+n2*(j+n1*i)] = float(value.replace('D', 'E'))

out /= np.max(out)

from scipy.ndimage.filters import gaussian_filter
out = np.reshape(out, (n0,n1))
out = gaussian_filter(out, 1, order=0, output=None, mode='reflect', cval=0.0)      
out = np.reshape(out, (n0*n1*n2))

out.tofile('.dat/conductivity.0.0.dat')
out.tofile('.dat/conductivity.1.0.dat')